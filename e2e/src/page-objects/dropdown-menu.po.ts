import { ElementFinder, browser, by, element, ElementArrayFinder } from 'protractor';

export class DropdownMenu {
    navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl) as Promise<unknown>;
    }
    getDropdownBtn(): ElementFinder {
        return element(by.name('dropdown-btn'));
    }
    getDropdownMenuContainer(): ElementFinder {
        return element(by.name('dropdown-container'));
    }
    getRadioBtnForNewerNotes(): ElementFinder {
        return element(by.name('radio-newer'));
    }
    getRadioBtnForOlderNotes(): ElementFinder {
        return element(by.name('radio-older'));
    }
    getCheckboxForEvenNotesDate(): ElementFinder {
        return element(by.name('filter-even-checkbox'));
    }
    getCheckBoxForOddNotesDate(): ElementFinder {
        return element(by.name('filter-odd-checkbox'));
    }
    getDateInputs(): ElementFinder {
        return element(by.name('date-input'));
    }
    public async EditNotes(type: string): Promise<void> {
        if (type == 'even') {
            await element.all(by.name('edit-note')).get(0).click();
            await this.getDateInputs().clear();
            await this.getDateInputs().sendKeys('18');
            await this.getDateInputs().sendKeys('11');
            await this.getDateInputs().sendKeys('2020');
            await element(by.name('save-changes')).click();
        }
        if (type == 'odd') {
            await element.all(by.name('edit-note')).get(1).click();
            await this.getDateInputs().clear();
            await this.getDateInputs().sendKeys('17');
            await this.getDateInputs().sendKeys('11');
            await this.getDateInputs().sendKeys('2020');
            await element(by.name('save-changes')).click();
        }
    }
}
