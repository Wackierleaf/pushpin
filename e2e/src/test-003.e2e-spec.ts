import { DropdownMenu } from './page-objects/dropdown-menu.po';

describe('Сортировка и фильтрация заметок:', () => {
    const page: DropdownMenu = new DropdownMenu();
    beforeAll(async () => {
        await page.navigateTo();
    });
    it('Отображение выпадающего меню', async () => {
        await page.getDropdownBtn().click();
        expect(page.getDropdownMenuContainer()?.isPresent()).toBeTruthy(
            'Отсутсвие выпадающего меню'
        );
    });
    it('Отображение содержимого выпадающего меню', async () => {
        expect(page.getRadioBtnForNewerNotes()?.isPresent()).toBeTruthy(
            'Не отображается радио кнопка для сортировки по убыванию'
        );
        expect(page.getRadioBtnForOlderNotes()?.isPresent()).toBeTruthy(
            'Не отображается радио кнопка для сортировки по возрастанию'
        );
        expect(page.getCheckboxForEvenNotesDate()?.isPresent()).toBeTruthy(
            'Не отображается чекбокс для отображения заметок с четными датами'
        );
        expect(page.getCheckBoxForOddNotesDate()?.isPresent()).toBeTruthy(
            'Не отображается чекбокс для отображения заметок с нечетными датами'
        );
    });
    it('Сортировка заметок', async () => {
        page.getDropdownBtn().click();
        await page.EditNotes('even');
         page.EditNotes('odd');
    });
});
